% Student exercise profile
:- set_prolog_flag(occurs_check, error).        % disallow cyclic terms
:- set_prolog_stack(global, limit(8 000 000)).  % limit term space (8Mb)
:- set_prolog_stack(local,  limit(2 000 000)).  % limit environment space

% Your program goes here
mzdelete(_, [], []).
mydelete(_, X, X).
mydelete(X, X, []).
mydelete(X, [H|T], RS):-
    X=:=H -> mydelete(X, T, RS); mydelete(X, T, TempRS), append(H, TempRS, RS).

%removeDuplicate([],[]).
%removeDuplicate([H|T], Clean):-
    

/** <examples> Your example queries go here, e.g.
?- member(X, [cat, mouse]).
*/
