% Student exercise profile
:- set_prolog_flag(occurs_check, error).        % disallow cyclic terms
:- set_prolog_stack(global, limit(8 000 000)).  % limit term space (8Mb)
:- set_prolog_stack(local,  limit(2 000 000)).  % limit environment space

% Your program goes here
reverse([],[]).
reverse([H|T], MyReverse):-
    reverse(T, RevT), append(RevT, [H], MyReverse).

% <examples> Your example queries go here, e.g.
% reverse([1,2,3,4,2,5],R)

